<?php

/**
 * @file
 */

/**
 * Before avoiding sending the email to a blocked email, you can specify that it
 * be sent anyway.
 *
 * @param bool $send
 *   By default, is FALSE.
 * @param array $context
 *   With the key:
 *     - email_blocked: The email blocked.
 *     - message: The message from hook_mail.
 */
function hook_asm_send_mail_email_blocked_alter(bool &$send, array $context) {
  $email = $context['email_blocked'];

  if ($email == 'admin@example.com') {
    $send = TRUE;
  }

  if ($context['message']['id'] == 'mymodule_somekey') {
    $send = TRUE;
  }
}
