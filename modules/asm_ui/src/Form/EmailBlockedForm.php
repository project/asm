<?php

namespace Drupal\asm_ui\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the mail blocked entity edit forms.
 */
class EmailBlockedForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New email blocked %label has been created.', $message_arguments));
        $this->logger('asm')->notice('Created new email blocked %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The email blocked %label has been updated.', $message_arguments));
        $this->logger('asm')->notice('Updated email blocked %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.asm_email_blocked.collection');

    return $result;
  }

}
