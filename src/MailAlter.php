<?php

namespace Drupal\asm;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

/**
 *
 */
class MailAlter implements MailAlterInterface {

  protected EntityStorageInterface $emailBlockedStorage;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, LoggerChannelInterface $logger) {
    $this->emailBlockedStorage = $entity_type_manager->getStorage('asm_email_blocked');
    $this->moduleHandler = $module_handler;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function alterMessage(array &$message): void {
    $emails = $this->parserEmails($message['to']);
    if (!empty($emails)) {
      $this->cleanEmails($emails, $message, 'to');

      // If there are other emails left, the "to" attribute is updated.
      if (!empty($emails)) {
        $message['to'] = implode(', ', $emails);
      }
      else {
        $message['send'] = FALSE;
      }
    }

    // If the email is sent in the same way, we check the Cc and Bcc headers.
    if ($message['send']) {
      $headers = &$message['headers'];

      if (!empty($headers['Cc'])) {
        $emails_cc = $this->parserEmails($headers['Cc']);
        if (!empty($emails_cc)) {
          $this->cleanEmails($emails_cc, $message, 'Cc');
          if (!empty($emails_cc)) {
            $headers['Cc'] = implode(', ', $emails_cc);
          }
        }
      }
      if (!empty($headers['Bcc'])) {
        $emails_bcc = $this->parserEmails($headers['Bcc']);
        if (!empty($emails_bcc)) {
          $this->cleanEmails($emails_bcc, $message, 'Bcc');
          if (!empty($emails_bcc)) {
            $headers['Bcc'] = implode(', ', $emails_bcc);
          }
        }
      }
    }
  }

  /**
   * @param array $emails
   * @param array $message
   * @param string $type
   */
  protected function cleanEmails(array &$emails, array $message, string $type): void {
    $blocked = [];
    $blocked_emails_id = $this->emailBlockedStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('email', array_keys($emails), 'IN')
      ->execute();

    if (!empty($blocked_emails_id)) {
      /** @var \Drupal\asm\EmailBlockedInterface[] $blocked_mails */
      $blocked_mails = $this->emailBlockedStorage->loadMultiple($blocked_emails_id);
      foreach ($blocked_mails as $blocked_mail) {
        $email_blocked = $blocked_mail->getEmail();

        $send = FALSE;
        $context = [
          'email_blocked' => $email_blocked,
          'message' => $message,
          'type' => $type,
        ];
        $this->moduleHandler->alter('asm_send_mail_email_blocked', $send, $context);

        if (!$send) {
          $blocked[$email_blocked] = $email_blocked;
          unset($emails[$email_blocked]);

        }
      }
    }

    if (!empty($blocked)) {
      $this->logger->debug('The following emails are removed from %type: %emails. This before sending the %id email. These emails are blocked.', [
        '%type' => $type,
        '%emails' => implode(', ', $blocked),
        '%id' => $message['id'],
      ]);
    }
  }

  /**
   * @param string $emails
   *
   * @return array
   */
  protected function parserEmails(string $emails): array {
    $results = [];
    $separated_mails = explode(',', $emails);
    foreach ($separated_mails as $original_mail) {
      $original_mail = trim($original_mail);
      $detail = $this->getEmailDetail($original_mail);
      if (isset($detail['full_email'])) {
        $results[$detail['full_email']] = $original_mail;
      }
    }

    return $results;
  }

  /**
   * @param string $email
   *
   * @return array
   */
  protected function getEmailDetail(string $email): array {
    $email_matches = [];

    $from_regex = '[a-zA-Z0-9_,\s\-\.\+\^!#\$%&*+\/\=\?\`\|\{\}~\']+';
    $user_regex = '[a-zA-Z0-9_\-\.\+\^!#\$%&*+\/\=\?\`\|\{\}~\']+';
    $domain_regex = '(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.?)+';
    $ipv4_regex = '[0-9]{1,3}(\.[0-9]{1,3}){3}';
    $ipv6_regex = '[0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7}';

    preg_match("/^$from_regex\s\<(($user_regex)@($domain_regex|(\[($ipv4_regex|$ipv6_regex)\])))\>$/", $email, $matches_2822);
    preg_match("/^($user_regex)@($domain_regex|(\[($ipv4_regex|$ipv6_regex)\]))$/", $email, $matches_normal);

    // Check for valid email as per RFC 2822 spec.
    if (empty($matches_normal) && !empty($matches_2822) && !empty($matches_2822[3])) {
      $email_matches['from_name'] = $matches_2822[0];
      $email_matches['full_email'] = $matches_2822[1];
      $email_matches['email_name'] = $matches_2822[2];
      $email_matches['domain'] = $matches_2822[3];
    }

    // Check for valid email as per RFC 822 spec.
    if (empty($matches_2822) && !empty($matches_normal) && !empty($matches_normal[2])) {
      $email_matches['from_name'] = '';
      $email_matches['full_email'] = $matches_normal[0];
      $email_matches['email_name'] = $matches_normal[1];
      $email_matches['domain'] = $matches_normal[2];
    }

    return $email_matches;
  }

}
