<?php

namespace Drupal\asm;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a mail blocked entity type.
 */
interface EmailBlockedInterface extends ContentEntityInterface {

  /**
   * @return string
   */
  public function getEmail(): string;

}
