<?php

namespace Drupal\asm;

/**
 *
 */
interface MailAlterInterface {

  /**
   * @param array $message
   *
   * @return void
   */
  public function alterMessage(array &$message): void;

}
