<?php

namespace Drupal\asm\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\asm\EmailBlockedInterface;

/**
 * Defines the mail blocked entity class.
 *
 * @ContentEntityType(
 *   id = "asm_email_blocked",
 *   label = @Translation("Email Blocked"),
 *   label_collection = @Translation("Emails Blocked"),
 *   label_singular = @Translation("email blocked"),
 *   label_plural = @Translation("emails blocked"),
 *   label_count = @PluralTranslation(
 *     singular = "@count mail blocked",
 *     plural = "@count mails blocked",
 *   ),
 *   handlers = {
 *     "storage_schema" = "Drupal\asm\EmailBlockedStorageSchema",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "asm_email_blocked",
 *   admin_permission = "administer asm email blocked",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "email",
 *     "uuid" = "uuid",
 *   },
 * )
 */
class EmailBlocked extends ContentEntityBase implements EmailBlockedInterface {

  /**
   * {@inheritdoc}
   */
  public function getEmail(): string {
    return $this->get('email')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setRequired(TRUE)
      ->addConstraint('UniqueField')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['reason'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Reason'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the mail blocked was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
