<?php

namespace Drupal\Tests\asm\Kernel;

use Drupal\asm\Entity\EmailBlocked;
use Drupal\KernelTests\KernelTestBase;

class MailAlterTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'text',
    'asm_test_headers_cc_bcc_mail',
    'asm',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('asm_email_blocked');
  }

  /**
   * Test emails blocked.
   */
  public function testEmailBlocked() {
    EmailBlocked::create([
      'email' => 'email_1@example.com',
    ])->save();
    EmailBlocked::create([
      'email' => 'email_2@example.com',
    ])->save();

    \Drupal::state()->set('system.test_mail_collector', []);

    \Drupal::service('plugin.manager.mail')->mail('default', 'default', 'email_1@example.com', 'en');
    \Drupal::service('plugin.manager.mail')->mail('default', 'default', 'email_2@example.com', 'en');

    $captured_emails = \Drupal::state()->get('system.test_mail_collector');
    $this->assertEmpty($captured_emails);

    \Drupal::service('plugin.manager.mail')->mail('default', 'default', 'email_1@example.com, email_2@example.com', 'en');
    $captured_emails = \Drupal::state()->get('system.test_mail_collector');
    $this->assertEmpty($captured_emails);

    \Drupal::service('plugin.manager.mail')->mail('default', 'default', 'email_3@example.com', 'en');
    $captured_emails = \Drupal::state()->get('system.test_mail_collector');
    $this->assertNotEmpty($captured_emails);

    \Drupal::state()->set('system.test_mail_collector', []);
    \Drupal::service('plugin.manager.mail')->mail('default', 'default', 'email_1@example.com, email_2@example.com, email_3@example.com', 'en');
    $captured_emails = \Drupal::state()->get('system.test_mail_collector');
    $this->assertNotEmpty($captured_emails);
    $this->assertEquals('email_3@example.com', $captured_emails[0]['to']);
  }

  /**
   * Test emails blocked on Cc and Bcc destination.
   */
  public function testEmailBlockedOnCcAndBccDestination() {
    EmailBlocked::create([
      'email' => 'email_1_cc@example.com',
    ])->save();
    \Drupal::state()->set('system.test_mail_collector', []);

    \Drupal::service('plugin.manager.mail')->mail('asm_test_headers_cc_bcc_mail', 'default', 'email_1@example.com', 'en', [
      'cc' => 'email_1_cc@example.com, email_2_cc@example.com',
    ]);
    $captured_emails = \Drupal::state()->get('system.test_mail_collector');
    $this->assertNotEmpty($captured_emails);
    $this->assertEquals('email_2_cc@example.com', $captured_emails[0]['headers']['Cc']);
    \Drupal::state()->set('system.test_mail_collector', []);

    \Drupal::service('plugin.manager.mail')->mail('asm_test_headers_cc_bcc_mail', 'default', 'email_1@example.com', 'en', [
      'bcc' => 'email_1_cc@example.com, email_2_cc@example.com',
    ]);
    $captured_emails = \Drupal::state()->get('system.test_mail_collector');
    $this->assertNotEmpty($captured_emails);
    $this->assertEquals('email_2_cc@example.com', $captured_emails[0]['headers']['Bcc']);
  }
}
